# Copyright 2017-2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'plex-media-player-1.3.4.670.ebuild' from Gentoo, which is:
# Copyright 1999-2017 Gentoo Foundation

COMMIT="7d18e33d"

require github [ user=plexinc tag=v${PV}-${COMMIT} ]
require cmake [ api=2 ]
require gtk-icon-cache

SUMMARY="Next generation Plex Desktop/Embedded Client"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"

MYOPTIONS="
    joystick
    lirc
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-lang/python:*
        dev-util/google-breakpad
        media/mpv
        sys-libs/zlib
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXrandr
        x11-libs/qtbase:5[>=5.7.1][gui]
        x11-libs/qtwebchannel:5[>=5.7.1&<5.11]
        x11-libs/qtwebengine:5[>=5.7.1&<5.11]
        x11-libs/qtdeclarative:5[>=5.7.1&<5.11]
        x11-libs/qtx11extras:5[>=5.7.1&<5.11]
        joystick? ( media-libs/SDL:2 )
        lirc? ( remote/lirc )
        providers:gnutls? ( media/ffmpeg[>=3.0][providers:gnutls] )
        providers:libressl? ( media/ffmpeg[>=3.0][providers:libressl] )
        providers:openssl? ( media/ffmpeg[>=3.0][providers:openssl] )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # Requires unpackaged libcec
    -DENABLE_CEC:BOOL=false

    -DFULL_GIT_REVISION:STRING="${COMMIT}"
    -DQTROOT:STRING="/usr/share/qt5"
    # Searches for the webclient files in ${CMAKE_INSTALL_PREFIX}/share
    -DCMAKE_INSTALL_PREFIX:PATH="/usr"
    -DCMAKE_INSTALL_BINDIR:PATH="/usr/$(exhost --target)/bin"
)

CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'joystick SDL2'
    'lirc LIRC'
)

MY_PNV="${PNV}-${COMMIT}"

WORK="${WORKBASE}/${MY_PNV}"

src_prepare() {
    cmake_src_prepare

    # Fails to find the required libX11
    edo sed -i -e "s:find_package(X11)::" CMakeModules/LinuxConfiguration.cmake
}

src_configure() {
    # Instead of downloading the additional stuff required for setup via conan
    # they now download them via CMake. Sadly manually fetching them doesn't seem
    # to work, since upstream changes the versions of the webclient stuff which is
    # fetched here at will, without increasing the version number of this package
    esandbox disable_net

    cmake_src_configure

    esandbox enable_net
}

